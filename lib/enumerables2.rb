require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0, :+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |word| word.include? substring }
end
# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  ('a'..'z').select { |a| string.count(a) > 1 }
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def word_length(word)
  punctuation = %w[! , . / : ; ' ?]
  punctuation.reduce(word.size) { |acc, a| acc - word.count(a) }
end

def length_sort(array)
  array.sort_by { |word| word_length(word) }
end

def longest_two_words(string)
  length_sort(string.split(' ')).reverse[0..1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ('a'..'z').reject { |a| string.include? a }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select { |year| not_repeat_year?(year) }
end

def not_repeat_year?(year)
  year.to_s.split('') == year.to_s.split('').uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.select { |song| no_repeats?(song, songs) }.uniq
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |song, idx|
    next if song != song_name || idx.zero?
    return false if song == songs[idx - 1]
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  words = string.split(' ').map { |word| remove_punctuation(word) }
  return '' unless string.downcase.include? 'c'
  words.reduce do |acc, word|
    c_distance(word) < c_distance(acc) ? word : acc
  end
end

def remove_punctuation(word)
  word.delete "!,./:;'?"
end

def c_distance(word)
  word.size - word.downcase.rindex('c').to_i
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]
def gets_end_idx(start_idx, arr)
  idx = start_idx + 1
  return false if (idx > arr.size) || (arr[start_idx] != arr[idx])
  idx += 1 until arr[idx + 1] != arr[start_idx]
  idx
end

def repeated_number_ranges(arr)
  new_arr = []
  idx = 0
  while idx < arr.size
    if gets_end_idx(idx, arr)
      new_arr << [idx, gets_end_idx(idx, arr)]
      idx = gets_end_idx(idx, arr)
    else
      idx += 1
    end
  end
  new_arr
end
